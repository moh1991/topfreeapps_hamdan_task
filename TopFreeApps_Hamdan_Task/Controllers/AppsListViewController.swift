//
//  AppsListViewController.swift
//  TopFreeApps_Hamdan_Task
//
//  Created by Mohammad Hamdan on 1/21/18.
//  Copyright © 2018 Mohammad Hamdan. All rights reserved.
//

import UIKit
import SCLAlertView

class AppsListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Properties
    @IBOutlet weak var tableView: UITableView!
    var appsArray: [AppDataModel]?
    fileprivate static let cellId = "AppItemTableViewCell"

    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup interface
        self.configureUI()
        
        // get apps request
        self.getApps()
        
    }
 
    // MARK: - Interface
    func configureUI() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        let nibName = UINib(nibName:Constants.Nibs.APP_ITEM_TABLE_VIEW_CELL, bundle:nil)
        tableView.register(nibName, forCellReuseIdentifier: Constants.Nibs.APP_ITEM_TABLE_VIEW_CELL)
    }
    
    
    // MARK: - Network Requests
    func getApps() {
        if BaseViewController.internetConnection == true {
            showHUD()
            APIManager.getApps { (response, errorType, errorMessage) in
                self.hideHUD()
                if errorType == .ResponseErrorTypeNone {
                    
                    if let parsedResponse: AppsResponse = response as?  AppsResponse {
                        if let array = parsedResponse.items {
                            self.appsArray =  [AppDataModel]()
                            for item in array {
                                self.appsArray?.append(item)
                            }
                            self.tableView.reloadData()
                        }
                    }
                } else {
                    self.showAlert(withType: .Error, message: "Error", title: errorMessage ?? "")
                }
            }
        } else {
            self.showAlert(withType: .Error, message: "Error", title: "No internet connection!")
        }
    }
    
    
    // MARK: - Table View Delegate and Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if let appsArr = self.appsArray {
            return appsArr.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: AppsListViewController.cellId, for: indexPath) as! AppItemTableViewCell

        let obj = appsArray![indexPath.row]
        cell.setipCell(withAppData: obj)
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let appItem =  appsArray![indexPath.row]
       
        let navVC  = storyboard?.instantiateViewController(withIdentifier: "navAppDetails") as! UINavigationController
        let detailVC : AppDetailsViewController = (navVC.topViewController as? AppDetailsViewController)!
        detailVC.appItem = appItem
       
        self.present(navVC, animated: true, completion: nil)
    }
    
    
}
