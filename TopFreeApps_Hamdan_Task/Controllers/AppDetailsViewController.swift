//
//  AppDetailsViewController.swift
//  TopFreeApps_Hamdan_Task
//
//  Created by Mohammad Hamdan on 1/22/18.
//  Copyright © 2018 Mohammad Hamdan. All rights reserved.
//

import UIKit
import SKPhotoBrowser

class AppInfoCell: UITableViewCell {
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
}

class AppDetailsViewController: UITableViewController {

    // MARK: - Properties
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var informationTableView: UITableView!
    
    var appItem : AppDataModel?
    var additionalInfo: [(type: String, value: String)]?
    lazy var infoDataSource: UITableViewDataSource = {
        self.additionalInfo = [
            (type: "Artist:", value: appItem?.artist ?? "Unknown"),
            (type: "Category:", value: appItem?.category ?? "Unknown"),
            (type: "Release Date:", value: appItem?.releaseDate ?? "Unknown"),
            (type: "Rights:", value: appItem?.rights ?? "Unknown")]
        return AppInfoDatasource(info: additionalInfo!)
    }()
    
    
    var appImages = [SKPhoto]()
    
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        self.navigationItem.title = "App Details"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                                 target: self,
                                                                 action: #selector(AppDetailsViewController.onDismiss))
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    // MARK: - Interface
    func configureUI() {

        appNameLabel?.text = appItem?.detailName
        summaryLabel?.text = appItem?.summary
        informationTableView?.dataSource = infoDataSource
        informationTableView?.rowHeight = UITableViewAutomaticDimension
        bannerImageView?.layer.cornerRadius = 10.0
        bannerImageView?.layer.masksToBounds = true
        bannerImageView.kf.indicatorType = .activity
        //bannerImageView.tag = i
        bannerImageView.kf.setImage(with: appItem?.bannerURL, placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (image, error, cachType, url) in
            
            if error == nil {
                let photo = SKPhoto.photoWithImage(image!)// add some UIImage
                self.appImages.append(photo)
            }
        })
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.imagePressed(_:)))
        bannerImageView.addGestureRecognizer(tapGesture)

        self.tableView.setNeedsLayout()
    }
    
    @objc func onDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Tap Gestures Handlers
    @objc func imagePressed(_ sender: UITapGestureRecognizer) {
        print("Please Help!")
        
        // 2. create PhotoBrowser Instance, and present from your viewController.
        let browser = SKPhotoBrowser(photos: appImages)
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    
    
    // MARK: - Buttons Handlers

    @IBAction func btnGoToLinkPressed(_ sender: Any) {
        let string = appItem?.link ?? ""
        print(string)
        if let url = URL(string : appItem?.link ?? "")  {
            UIApplication.shared.open(url, options: [:])
        }
//
//        let appLink = "http://itunes.apple.com/app/id\((appItem?.appstoreID)!)"
//        print(appLink)
//        if let url = URL(string : appLink){//appItem?.link ?? "")  {
//            UIApplication.shared.open(url, options: [:])
//        }
        
        
    }
    
    
    // MARK: - Table View Delegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}


// MARK: - App Info Datasource

class AppInfoDatasource: NSObject, UITableViewDataSource {
    static let cellID = "InfoCellID"
    
    let info: [(type: String, value: String)]
    
    init(info: [(type: String, value: String)]) {
        self.info = info
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return info.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellInfo = info[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: self).cellID) as! AppInfoCell
        
        cell.typeLabel.text = cellInfo.type
        cell.valueLabel.text = cellInfo.value
        cell.setNeedsLayout()
        return cell
    }
}
