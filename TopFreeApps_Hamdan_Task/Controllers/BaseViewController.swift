//
//  BaseViewController.swift
//  TopFreeApps_Hamdan_Task
//
//  Created by Mohammad Hamdan on 1/21/18.
//  Copyright © 2018 Mohammad Hamdan. All rights reserved.
//

import UIKit
import MBProgressHUD
import SCLAlertView

class BaseViewController: UIViewController {
    
    var spinnerActivity: MBProgressHUD?
    static var reach: Reachability?
    static var internetConnection : Bool = false
    
    
    // MARK: - Reachability
    func rechability() {
        // Allocate a reachability object
        BaseViewController.reach = Reachability.forInternetConnection()
        
        // Here we set up a NSNotification observer. The Reachability that caused the notification
        // is passed in the object parameter
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(reachabilityChanged),
            name: NSNotification.Name.reachabilityChanged,
            object: nil
        )
        
        BaseViewController.reach!.startNotifier()
    }
    
    @objc func reachabilityChanged() {
        
        if BaseViewController.reach!.isReachableViaWiFi() || BaseViewController.reach!.isReachableViaWWAN() {
            print("Service avalaible!!!")
            BaseViewController.internetConnection = true
        } else {
            print("No service avalaible!!!")
            BaseViewController.internetConnection = false
        }
        
    }
    
    // MARK: - HUD
    func initHUD() {
        spinnerActivity!.isUserInteractionEnabled = false;
        spinnerActivity!.bezelView.color = UIColor.black
        spinnerActivity!.bezelView.backgroundColor = UIColor.black
        spinnerActivity!.backgroundView.color = UIColor(white: 0.3, alpha: 0.4)
        spinnerActivity!.contentColor = UIColor.white
        
    }
    func showHUD() {
        spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true);
    }
    
    func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    // MARK: - Alerts
    func showAlert(withType alertType: AlertViewType, message: String?, title: String){
        
        switch alertType {
        case .Success:
            SCLAlertView().showSuccess(message!, subTitle: title, closeButtonTitle: "Ok") // Error
        case .Error:
            SCLAlertView().showError(message!, subTitle: title, closeButtonTitle: "Ok") // Error
        case .Notice:
            SCLAlertView().showNotice(message!, subTitle: title,  closeButtonTitle: "Ok") // Notice
        case .Warning:
            SCLAlertView().showWarning(message!, subTitle: title,  closeButtonTitle: "Ok") // Warning
        case .info:
            SCLAlertView().showInfo(message!, subTitle: title,  closeButtonTitle: "Ok") // Info
        case .Edit:
            SCLAlertView().showEdit(message!, subTitle: title,  closeButtonTitle: "Ok") // Edit
            
        }
        
    }
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.rechability()
        self.reachabilityChanged()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.isTranslucent = false
    }
}
