//
//  AppDataModel.swift
//  TopFreeApps_Hamdan_Task
//
//  Created by Mohammad Hamdan on 1/21/18.
//  Copyright © 2018 Mohammad Hamdan. All rights reserved.
//

import UIKit
import SwiftyJSON

class AppDataModel: NSObject {
    
    var appstoreID: String?
    var shortName: String?
    var detailName: String?
    var artist: String?
    var category: String?
    var releaseDate: String?
    var summary: String?
    var rights: String?
    var price: String?
    var link: String?
    var iconURL: URL?
    var bannerURL: URL?

    init(json:JSON) {
        
        self.appstoreID        = json["id"]["attributes"]["im:id"].string
        self.shortName         = json["im:name"]["label"].string
        self.detailName        = json["title"]["label"].string
        self.artist            = json["im:artist"]["label"].string
        self.category          = json["category"]["attributes"]["term"].string
        self.releaseDate       = json["im:releaseDate"]["attributes"]["label"].string
        self.summary           = json["summary"]["label"].string
        self.rights            = json["rights"]["label"].string
        self.price             = "\(json["im:price"]["attributes"]["amount"].string ?? "0") \(json["im:price"]["attributes"]["currency"].string ?? "") "
        
        self.link              = json["link"]["attributes"]["href"].string
        if let imagesArray = json["im:image"].array {
            self.iconURL = imagesArray.first?["label"].url
            self.bannerURL = imagesArray.last?["label"].url
        }

    }
    
    
}

