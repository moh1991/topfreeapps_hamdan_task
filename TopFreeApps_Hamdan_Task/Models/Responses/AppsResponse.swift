//
//  AppsResponse.swift
//  TopFreeApps_Hamdan_Task
//
//  Created by Mohammad Hamdan on 1/21/18.
//  Copyright © 2018 Mohammad Hamdan. All rights reserved.
//

import UIKit
import SwiftyJSON

class AppsResponse: NSObject {
    
    var items : [AppDataModel]?
    
    init(json :JSON) {
        if let feedDict = json["feed"].dictionary {
            
            if let entryArray = feedDict["entry"]?.array {
                
                items = [AppDataModel]()
                
                for item in entryArray {
                    let Obj = AppDataModel.init(json: item)
                    items?.append(Obj)
                }
            }
        }
    }
}
