//
//  AppItemTableViewCell.swift
//  TopFreeApps_Hamdan_Task
//
//  Created by Mohammad Hamdan on 1/21/18.
//  Copyright © 2018 Mohammad Hamdan. All rights reserved.
//

import UIKit
import Kingfisher

class AppItemTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblAppTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK : - Cell Configuration
    func setipCell(withAppData app: AppDataModel) {
        
        self.lblAppTitle.text = app.shortName
        self.imgView.contentMode = .scaleAspectFit
        self.imgView?.layer.cornerRadius = 10
        self.imgView?.layer.masksToBounds = true

        self.imgView.kf.indicatorType = .activity
        imgView.kf.indicatorType = .activity
        imgView.kf.setImage(with: app.iconURL, placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
    }
    
}
