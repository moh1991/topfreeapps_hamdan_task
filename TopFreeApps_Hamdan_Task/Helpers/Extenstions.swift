//
//  Extenstions.swift
//  TopFreeApps_Hamdan_Task
//
//  Created by Mohammad Hamdan on 1/21/18.
//  Copyright © 2018 Mohammad Hamdan. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UIView Extension

extension UIView {
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    
    func addConstraintsWithFormat(format : String, views : UIView...) {
        
        var viewsDictionary = [String: UIView]()
        
        for (index, view) in views.enumerated() {
            
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
            
        }
        
        var constraints = [NSLayoutConstraint]()
        constraints += NSLayoutConstraint.constraints(withVisualFormat: format, options: [], metrics: nil, views: viewsDictionary)
        NSLayoutConstraint.activate(constraints)
        
    }
    
    func dropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}


// MARK: - UIFont Extension

extension UIFont {
    static func customArabicRegularFont(withSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: Constants.Fonts.CUSTOM_AR_FONTNAME_REG, size: fontSize)!
    }
    
    static func customArabicBoldFont(withSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: Constants.Fonts.CUSTOM_AR_FONTNAME_BOLD, size: fontSize)!
    }
    
    static func customEnglishRegularFont(withSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: Constants.Fonts.CUSTOM_EN_FONTNAME_REG, size: fontSize)!
    }
    
    static func customEnglishBoldFont(withSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: Constants.Fonts.CUSTOM_EN_FONTNAME_BOLD, size: fontSize)!
    }
    
}

// MARK: - CGFloat Extension

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

// MARK: - UIColor Extension

extension UIColor {
    
    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }

    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
    
}

extension Double {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}




