//
//  Constants.swift
//  TopFreeApps_Hamdan_Task
//
//  Created by Mohammad Hamdan on 1/21/18.
//  Copyright © 2018 Mohammad Hamdan. All rights reserved.
//


import Foundation
import UIKit

struct Constants {
    
    struct URLs {
        static let BASE_URL            = "https://itunes.apple.com/jo/";
        static let TOP_N_APPS_URL      = "rss/topfreeapplications/limit";
        static let TOP_50_APPS_URL     = "rss/topfreeapplications/limit=50/json";
    }
    
    struct Nibs {
        static let APP_ITEM_TABLE_VIEW_CELL     = "AppItemTableViewCell"
    }
    
    struct Color {
        static let primaryColor = UIColor.init(hex: "ff3333")
        static let secondaryColor = UIColor.lightGray
    }
    
    struct Fonts {
        static let CUSTOM_AR_FONTNAME_REG: String     = "DroidArabicKufi"
        static let CUSTOM_AR_FONTNAME_BOLD: String    = "DroidArabicKufi-Bold"
        static let CUSTOM_EN_FONTNAME_REG: String     = "DroidArabicKufi"
        static let CUSTOM_EN_FONTNAME_BOLD: String    = "DroidArabicKufi-Bold"
    }
    
    struct ScreenSize {
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6_7          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P_7P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
        static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
    }
    
    struct Version{
        static let SYS_VERSION_FLOAT = (UIDevice.current.systemVersion as NSString).floatValue
        static let iOS7 = (Version.SYS_VERSION_FLOAT < 8.0 && Version.SYS_VERSION_FLOAT >= 7.0)
        static let iOS8 = (Version.SYS_VERSION_FLOAT >= 8.0 && Version.SYS_VERSION_FLOAT < 9.0)
        static let iOS9 = (Version.SYS_VERSION_FLOAT >= 9.0 && Version.SYS_VERSION_FLOAT < 10.0)
        static let iOS10 = (Version.SYS_VERSION_FLOAT >= 10.0 && Version.SYS_VERSION_FLOAT < 11.0)
        static let iOS11 = (Version.SYS_VERSION_FLOAT >= 11.0 && Version.SYS_VERSION_FLOAT < 12.0)
    }
}

// MARK: - Enums
public enum AlertViewType {
    case Success
    case Error
    case Notice
    case Warning
    case info
    case Edit
}

public enum ResponseErrorType : String {
    case ResponseErrorTypeUnsupportedResponseClass = "Not Supported"
    case ResponseErrorTypeBadRequest = "400" // 400 - check parameters
    case ResponseErrorTypeLoginRequired = "401" // 401 - show login page
    case ResponseErrorTypePaymentRequired = "402"// 402 - payment required
    case ResponseErrorTypeForbidden = "403"// 403 - forbidden
    case ResponseErrorTypeNotFound = "404"// 404 - not found
    case ResponseErrorTypeGeneralServerError  = "General HTTP Error"// http error code not handled
    case ResponseErrorTypeNone = "200 - status = OK"// 200 - status = OK
    case ResponseErrorTypeOther = "200 - status = FAILED"// 200 - status = FAILED
    case ResponseErrorTypeGeneralNoResponse = "General No Response"
    case ResponseErrorTypeNoConnection = "No Connection"// no connection
}

