//
//  NetworkManager.swift
//  TopFreeApps_Hamdan_Task
//
//  Created by Mohammad Hamdan on 1/21/18.
//  Copyright © 2018 Mohammad Hamdan. All rights reserved.
//

import UIKit

import Foundation
import Alamofire
import SwiftyJSON


class NetworkManager: NSObject {
    
    static func nerworkRequester(_ urlString: String,
                                 httpMethod: Alamofire.HTTPMethod,
                                 parameters params: [String: String]?,
                                 completionHandler: @escaping (
        _ responseData: Any?,
        _ errorType : ResponseErrorType,
        _ errorMessage : String?) -> Void) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request(urlString, method: httpMethod, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                // Success
                if response.result.isSuccess {
                    
                    if let data = response.result.value {
                        // parse JSON
                        let json = JSON(data)
                        
                        // Print JSON
                        print(json)
                        
                        completionHandler(json, ResponseErrorType.ResponseErrorTypeNone, nil)
                        
                    }
                }  else if response.result.isFailure {
                    let error : Error = response.result.error!
                    print(error.localizedDescription    )
                    //  errorBlock(error)
                    completionHandler(error.localizedDescription, ResponseErrorType.ResponseErrorTypeGeneralNoResponse, error.localizedDescription)
                }
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        
    }
    
    //MARK: - GET
    class func getRequest(_ urlString: String,parameters params: [String: String]?, headers: [String: String]?, completionHandler: @escaping (_ responseData: Any?, _ errorType : ResponseErrorType, _ errorMessage : String?) -> Void) {
        nerworkRequester(urlString, httpMethod: HTTPMethod.get, parameters: params) { (responseJson, errorType, errorMesssage) in
            completionHandler(responseJson, errorType, errorMesssage)
        }
    }
    
    //MARK: - POST
    class  func postRequest(_ urlString: String, parameters params: [String: String]?, headers: [String: String]?, completionHandler: @escaping (_ responseData: Any?, _ errorType : ResponseErrorType, _ errorMessage : String?) -> Void) {
        nerworkRequester(urlString, httpMethod: HTTPMethod.post, parameters: params) { (responseJson, errorType, errorMesssage) in
            
            completionHandler(responseJson, errorType, errorMesssage)
        }
    }
    
    //MARK: - PUT
    class  func putRequest(_ urlString: String ,parameters params: [String: String]?, headers: [String: String]?, completionHandler: @escaping (_ responseData: Any?, _ errorType : ResponseErrorType, _ errorMessage : String?) -> Void) {
        nerworkRequester(urlString, httpMethod: HTTPMethod.put, parameters: params) { (responseJson, errorType, errorMesssage) in
            completionHandler(responseJson, errorType, errorMesssage)
        }
    }
    
    //MARK: - DELETE
    class  func deleteRequest(_ urlString: String ,parameters params: [String: String]?, headers: [String: String]?, completionHandler: @escaping (_ responseData: Any?, _ errorType : ResponseErrorType, _ errorMessage : String?) -> Void) {
        nerworkRequester(urlString, httpMethod: HTTPMethod.delete, parameters: params) { (responseJson, errorType, errorMesssage) in
            completionHandler(responseJson, errorType, errorMesssage)
        }
    }
    
}



