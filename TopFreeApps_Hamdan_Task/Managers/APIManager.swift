//
//  APIManager.swift
//  TopFreeApps_Hamdan_Task
//
//  Created by Mohammad Hamdan on 1/21/18.
//  Copyright © 2018 Mohammad Hamdan. All rights reserved.
//

import UIKit
import SwiftyJSON

class APIManager: NSObject {
  
    class func getApps(withCompletionHandler completionHandler: @escaping (_ responseData: Any?, _ errorType : ResponseErrorType, _ errorMessage : String?) -> Void) {
        
        let urlString = Constants.URLs.BASE_URL +  Constants.URLs.TOP_50_APPS_URL
        
        NetworkManager.getRequest(urlString, parameters: nil, headers: nil) { (jsonResponseData, errorType, errorMessage) in
            
            if errorType == .ResponseErrorTypeNone {
                
                if let json = jsonResponseData as? JSON {
                    let parsedResponse: AppsResponse? = AppsResponse.init(json: json)
                    completionHandler(parsedResponse, errorType, errorMessage)
                }
                else {
                    completionHandler(0, errorType, errorMessage)
                }
            }
            else {
                completionHandler(0, errorType, errorMessage)
            }
        }
        
    }
}
